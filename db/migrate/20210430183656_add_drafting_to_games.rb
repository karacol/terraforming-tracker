class AddDraftingToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :drafting, :boolean
  end
end
