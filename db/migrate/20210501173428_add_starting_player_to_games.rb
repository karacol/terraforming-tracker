class AddStartingPlayerToGames < ActiveRecord::Migration[6.0]
  def change
    change_table :games do |t|
      t.belongs_to :starting_player
    end
  end
end
