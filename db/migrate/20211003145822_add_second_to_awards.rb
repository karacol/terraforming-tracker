class AddSecondToAwards < ActiveRecord::Migration[6.0]
  def change
    add_reference :awards, :second
  end
end
