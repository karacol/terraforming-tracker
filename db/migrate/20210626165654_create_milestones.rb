class CreateMilestones < ActiveRecord::Migration[6.0]
  def change
    create_table :milestones do |t|
      t.string :name
      t.boolean :reached
      t.belongs_to :owner
      t.belongs_to :game

      t.timestamps
    end
  end
end
