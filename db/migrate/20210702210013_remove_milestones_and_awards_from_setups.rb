class RemoveMilestonesAndAwardsFromSetups < ActiveRecord::Migration[6.0]
  def change
    remove_column :setups, :milestones, :text
    remove_column :setups, :awards, :text
  end
end
