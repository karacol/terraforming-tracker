class AddExpansionAdvancedToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :expansion_advanced, :boolean
  end
end
