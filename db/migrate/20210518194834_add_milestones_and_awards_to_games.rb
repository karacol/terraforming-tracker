class AddMilestonesAndAwardsToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :milestones, :text
    add_column :games, :awards, :text
  end
end
