class AddMoreDetailsToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :milestones, :text
    add_column :setups, :awards, :text
    add_column :setups, :prelude_cards, :text
  end
end
