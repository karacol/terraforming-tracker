class RemoveRedundantColumnsFromGames < ActiveRecord::Migration[6.0]
  def change
    remove_column :games, :prelude_cards, :text
    remove_column :games, :milestones, :text
    remove_column :games, :awards, :text
  end
end
