class RemoveStartAndEndFromGames < ActiveRecord::Migration[6.0]
  def change
    remove_column :games, :start, :time
    remove_column :games, :end, :time
  end
end
