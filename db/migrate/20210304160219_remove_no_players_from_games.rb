class RemoveNoPlayersFromGames < ActiveRecord::Migration[6.0]
  def change
    remove_column :games, :no_players, :integer
  end
end
