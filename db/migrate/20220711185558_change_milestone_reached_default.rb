class ChangeMilestoneReachedDefault < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:milestones, :reached, from: nil, to: false)
  end
end
