class RemoveExpansionHellasElysiumFromGames < ActiveRecord::Migration[6.0]
  def change
    remove_column :games, :expansion_hellas_elysium, :boolean
  end
end
