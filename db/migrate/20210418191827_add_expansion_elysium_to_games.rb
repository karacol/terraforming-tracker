class AddExpansionElysiumToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :expansion_elysium, :boolean
  end
end
