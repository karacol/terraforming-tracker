class AddExpansionHellasToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :expansion_hellas, :boolean
  end
end
