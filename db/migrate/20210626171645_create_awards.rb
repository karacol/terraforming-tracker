class CreateAwards < ActiveRecord::Migration[6.0]
  def change
    create_table :awards do |t|
      t.string :name
      t.boolean :financed, default: false
      t.belongs_to :owner
      t.belongs_to :game

      t.timestamps
    end
  end
end
