class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
      t.integer :no_players
      t.time :start
      t.time :end
      t.integer :score
      t.string :players
      t.integer :generations

      t.timestamps
    end
  end
end
