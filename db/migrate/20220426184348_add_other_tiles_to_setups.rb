class AddOtherTilesToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :other_tiles, :integer
  end
end
