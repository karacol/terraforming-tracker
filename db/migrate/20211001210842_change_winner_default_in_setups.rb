class ChangeWinnerDefaultInSetups < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:setups, :winner, from: nil, to: false)
  end
end
