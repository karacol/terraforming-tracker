class AddDetailsToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :green_tiles, :integer
    add_column :setups, :city_tiles, :integer
    add_column :setups, :total_tiles, :integer
    add_column :setups, :cash_production, :integer
    add_column :setups, :steel_production, :integer
    add_column :setups, :titanium_production, :integer
    add_column :setups, :plant_production, :integer
    add_column :setups, :energy_production, :integer
    add_column :setups, :heat_production, :integer
  end
end
