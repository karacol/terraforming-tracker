class AddSecondToMilestones < ActiveRecord::Migration[6.0]
  def change
    add_reference :milestones, :second
  end
end
