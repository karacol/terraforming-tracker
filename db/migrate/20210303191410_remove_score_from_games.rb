class RemoveScoreFromGames < ActiveRecord::Migration[6.0]
  def change
    remove_column :games, :score, :string
    remove_column :games, :integer, :string
  end
end
