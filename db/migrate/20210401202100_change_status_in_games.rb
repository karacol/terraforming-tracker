class ChangeStatusInGames < ActiveRecord::Migration[6.0]
  def change
    change_column_default :games, :status, from: 'running', to: 'setting up'
  end
end
