class RemovePlayersFromGames < ActiveRecord::Migration[6.0]
  def change
    remove_column :games, :players, :string
    remove_column :games, :string, :string
  end
end
