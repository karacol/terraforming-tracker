class RemoveSecondFromMilestones < ActiveRecord::Migration[6.0]
  def change
    remove_reference :milestones, :second
  end
end
