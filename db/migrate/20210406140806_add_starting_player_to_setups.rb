class AddStartingPlayerToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :starting_player, :boolean, default: false
  end
end
