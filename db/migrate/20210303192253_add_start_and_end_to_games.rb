class AddStartAndEndToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :start, :datetime, precision: 6
    add_column :games, :end, :datetime, precision: 6
  end
end
