class RemoveTotalTilesFromSetup < ActiveRecord::Migration[6.0]
  def change
    remove_column :setups, :total_tiles, :integer
  end
end
