class AddDetailsToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :expansion_turmoil, :boolean
    add_column :games, :expansion_colonies, :boolean
    add_column :games, :expansion_prelude, :boolean
    add_column :games, :expansion_hellas_elysium, :boolean
    add_column :games, :expansion_venus_next, :boolean
    add_column :games, :cap_oxygen, :integer
    add_column :games, :cap_ocean, :integer
    add_column :games, :cap_temperature, :integer
    add_column :games, :cap_venus, :integer
  end
end
