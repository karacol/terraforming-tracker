class ChangeTimesInGames < ActiveRecord::Migration[6.0]
  def change
    rename_column(:games, :start, :start_time)
    rename_column(:games, :end, :end_time)
  end
end
