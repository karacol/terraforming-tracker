class RemoveStartingPlayerFromSetups < ActiveRecord::Migration[6.0]
  def change
    remove_column :setups, :starting_player, :boolean
  end
end
