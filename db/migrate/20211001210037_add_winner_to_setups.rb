class AddWinnerToSetups < ActiveRecord::Migration[6.0]
  def change
    add_column :setups, :winner, :boolean
  end
end
