# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_11_185558) do

  create_table "awards", force: :cascade do |t|
    t.string "name"
    t.boolean "financed", default: false
    t.integer "owner_id"
    t.integer "game_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "second_id"
    t.index ["game_id"], name: "index_awards_on_game_id"
    t.index ["owner_id"], name: "index_awards_on_owner_id"
    t.index ["second_id"], name: "index_awards_on_second_id"
  end

  create_table "games", force: :cascade do |t|
    t.integer "generations"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "start_time", precision: 6
    t.datetime "end_time", precision: 6
    t.string "status", default: "setting up"
    t.boolean "expansion_turmoil"
    t.boolean "expansion_colonies"
    t.boolean "expansion_prelude"
    t.boolean "expansion_venus_next"
    t.integer "cap_oxygen"
    t.integer "cap_ocean"
    t.integer "cap_temperature"
    t.integer "cap_venus"
    t.boolean "expansion_hellas"
    t.boolean "expansion_elysium"
    t.boolean "expansion_advanced"
    t.boolean "drafting"
    t.text "colonies"
    t.integer "starting_player_id"
    t.index ["starting_player_id"], name: "index_games_on_starting_player_id"
  end

  create_table "milestones", force: :cascade do |t|
    t.string "name"
    t.boolean "reached", default: false
    t.integer "owner_id"
    t.integer "game_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_milestones_on_game_id"
    t.index ["owner_id"], name: "index_milestones_on_owner_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "setups", force: :cascade do |t|
    t.string "company"
    t.integer "score"
    t.integer "player_id", null: false
    t.integer "game_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "green_tiles"
    t.integer "city_tiles"
    t.integer "cash_production"
    t.integer "steel_production"
    t.integer "titanium_production"
    t.integer "plant_production"
    t.integer "energy_production"
    t.integer "heat_production"
    t.text "prelude_cards"
    t.boolean "winner", default: false
    t.integer "other_tiles"
    t.index ["game_id"], name: "index_setups_on_game_id"
    t.index ["player_id"], name: "index_setups_on_player_id"
  end

  add_foreign_key "setups", "games"
  add_foreign_key "setups", "players"
end
