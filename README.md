# README

Track the metadata of Terraforming Mars games and create beutiful graphs from all the data that was collected!

## Setup for development

### Ruby and Rails

**Note:** General installation instructions for most operating systems can be found [in section 3.1 of the "Getting Started" Ruby on Rails guide](https://guides.rubyonrails.org/getting_started.html#creating-a-new-rails-project-installing-rails).
The following steps have worked for my setups (Ubuntu + Raspbian with Docker).

#### ruby

I use version 2.7.0.

```
sudo apt-get install ruby-full
ruby --version
```

#### sqlite3

```
sudo apt-get update
sudo apt-get install -y sqlite3
sqlite3 --version
```

#### NodeJS v14

`yarn` was not too happy with NodeJS v16 in my setups, so stick with 14.

```
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs
node --version
```

#### Yarn

Caution: You might have to uninstall `cmdtest` first, as they also provide a package called yarn and this might cause conflicts.

```
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
sudo apt-get update
sudo apt-get install -y yarn
yarn --version
```

### Project

Pull the code, and navigate into the root directory.

### Bundler

Check if you already have bundler installed using `bundler --version`.
If you do, try to continue with the setup and only come back to this section if you run into problems with bundler and its version.

This project was bundled with bundler 2.1.4. To install this specific version, do `gem install bundler -v 2.1.4`.

### Dependencies

To install the gem dependencies, run `bundle install`. If you run into errors, read the messages and do as they say - you might need to install some more stuff.

To install JavaScript dependencies, run `yarn`.

### Database

Run `bundle e rails db:setup`.

### Check install

To find out whether what you did worked, run `bin/rails server` and navigate to [http://localhost:3000](http://localhost:3000) in your browser. You should see something!

## Docker

Install `docker` and `docker-compose`. If you have any issues or want to make changes to the Dockerfile or `docker-compose.yaml`, [this guide](https://docs.docker.com/samples/rails/) is really helpful.

```
docker-compose build
docker-compose up -d
```

This starts the container. The app takes a few minutes, but you should then be able to reach it on port 3080.

To run commands inside the container: `docker-compose run web <your-command>`.

To stop the container: `docker-compose down`.