require "application_system_test_case"

class GamesTest < ApplicationSystemTestCase
  test "visiting the index" do
    visit games_url

    assert_selector "h2", text: "Past games"
  end

  test "creating a game" do
    visit root_url
    click_on "Start a new game"
    take_screenshot
    check "Player One"
    click_on "Create Game"

    assert_text "Starting player"
  end
end
