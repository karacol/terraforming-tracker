require 'test_helper'

class PlayersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get players_url
    assert_response :success
  end

  test "should create player with unique name" do
    assert_difference("Player.count") do
      post players_url, params: { player: { name: "Queen Elizabeth II." }}
    end
    assert_redirected_to root_path
  end

  test "should not create player with non-unique name" do
    assert_no_difference("Player.count") do
      n = players(:PlayerOne).name
      post players_url, params: { player: { name: n }}
    end
    assert_response 422 
  end
end
