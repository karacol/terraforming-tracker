require 'test_helper'

class SetupsControllerTest < ActionDispatch::IntegrationTest
  test "should show setup" do
    setup = setups(:SetupOne)
    get setup_url(setup)
    assert_response :success
  end

  test "should destroy setup" do
    setup = setups(:SetupThree)
    g = setup.game
    assert_difference('g.players.count', -1) do
      assert_difference('Setup.count', -1) do
        delete setup_url(setup)
      end
    end
    assert_redirected_to root_url
  end
end
