require 'test_helper'

class GamesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get games_url
    assert_response :success
    assert_equal "index", @controller.action_name
  end

  test "should create game" do
    player_1 = players(:PlayerOne)
    player_2 = players(:PlayerTwo)
    assert_difference('Game.count') do
      post games_url, params: { game: { player_ids: [player_1.id, player_2.id] } }
    end
   
    assert_redirected_to game_setup_steps_path(Game.last)
  end

  test "should show game" do
    game = games(:GameOne)
    get game_url(game)
    assert_response :success
    assert_match game.start_time.to_s, @response.body
  end

  test "should destroy game and its components" do
    game = games(:GameOne)
    no_setups = game.setups.length
    no_milestones = game.milestones.length
    no_awards = game.awards.length
    assert_difference("Game.count", -1) do
      assert_difference("Setup.count", -1*no_setups) do
        assert_difference("Milestone.count", -1*no_milestones) do
          assert_difference("Award.count", -1*no_awards) do
            delete game_url(game)
          end
        end
      end
    end
  end
end
