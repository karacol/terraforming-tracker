require 'test_helper'

class GameFlowTest < ActionDispatch::IntegrationTest
  test "can see root page" do
    get "/"
    assert_select "h1", "Terraforming Mars Tracker"
    assert_select "h2", "Past games"
    assert_select "h2", "Expansions count"
  end

  test "can create a new game" do
    get "/games/new"
    assert_response :success

    player = players(:PlayerOne)

    post "/games",
      params: { game: { player_ids: [player.id] } }
    assert_response :redirect
    follow_redirect!
    assert_equal "setup_steps", path.split("/")[-1]
  end
end
