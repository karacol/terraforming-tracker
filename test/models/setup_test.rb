# == Schema Information
#
# Table name: setups
#
#  id                  :integer          not null, primary key
#  company             :string
#  score               :integer
#  player_id           :integer          not null
#  game_id             :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  green_tiles         :integer
#  city_tiles          :integer
#  cash_production     :integer
#  steel_production    :integer
#  titanium_production :integer
#  plant_production    :integer
#  energy_production   :integer
#  heat_production     :integer
#  prelude_cards       :text
#  winner              :boolean          default("0")
#  other_tiles         :integer
#

require 'test_helper'

class SetupTest < ActiveSupport::TestCase
  test "should not save without game" do 
    s = Setup.new
    s.player = players(:PlayerOne)
    assert_not s.save
  end

  test "should not save without player" do
    s = Setup.new
    s.game = games(:GameOne)
    assert_not s.save
  end

  test "should not save without company in running game" do
    s = setups(:SetupFive)  # no company
    s.game = games(:GameThree)  # running
    assert_not s.save
  end

  test "should not save without company in finishing game" do
    s = setups(:SetupFive)  # no company
    s.game = games(:GameFour)  # finishing
    assert_not s.save
  end  

  test "should not save without company in finished game" do
    s = setups(:SetupFive)  # no company
    s.game = games(:GameOne)  # finished
    assert_not s.save
  end

  test "should not save without prelude cards in game with prelude" do
    s = setups(:SetupSix)
    s.game.expansion_prelude = true
    assert_not s.save
  end
end
