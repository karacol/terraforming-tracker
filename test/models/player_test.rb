# == Schema Information
#
# Table name: players
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  test "should not save player without name" do
    p = Player.new
    assert_not p.save
  end

  test "should not save player with doubled name" do
    p = Player.new
    p.name = players(:PlayerOne).name
    assert_not p.save
  end

  test "winner logic" do
    p = players(:PlayerTwo)
    wins = p.get_all_wins
    assert_equal wins.length, 1
    gs = Game.all
    gs.each do |g|
      if wins.include?(g)
        assert p.winner?(g.id)
      else
        assert_not p.winner?(g.id)
      end
    end
  end
end
