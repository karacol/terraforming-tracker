# == Schema Information
#
# Table name: games
#
#  id                   :integer          not null, primary key
#  generations          :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  start_time           :datetime
#  end_time             :datetime
#  status               :string           default("setting up")
#  expansion_turmoil    :boolean
#  expansion_colonies   :boolean
#  expansion_prelude    :boolean
#  expansion_venus_next :boolean
#  cap_oxygen           :integer
#  cap_ocean            :integer
#  cap_temperature      :integer
#  cap_venus            :integer
#  expansion_hellas     :boolean
#  expansion_elysium    :boolean
#  expansion_advanced   :boolean
#  drafting             :boolean
#  colonies             :text
#  starting_player_id   :integer
#

require 'test_helper'

class GameTest < ActiveSupport::TestCase
  test "should not save without at least one player" do
    g = Game.new
    assert_not g.save
  end

  test "should not save without info once running" do
    g = Game.new
    g.status = "running"
    assert_not g.save
    info = [:start_time, :expansion_turmoil, :expansion_colonies, :expansion_prelude,
            :expansion_venus_next, :expansion_hellas, :expansion_elysium, :expansion_advanced,
            :drafting, :starting_player]
    info.each do |i|
      assert (g.errors.messages.keys.include? i), "Did not report missing #{i} on running game"
    end
  end

  test "should not save without info once finishing" do
    g = Game.new
    g.status = "finishing"
    assert_not g.save
    info = [:start_time, :expansion_turmoil, :expansion_colonies, :expansion_prelude,
            :expansion_venus_next, :expansion_hellas, :expansion_elysium, :expansion_advanced,
            :drafting, :starting_player, :cap_oxygen, :cap_ocean, :cap_temperature]
    info.each do |i|
      assert (g.errors.messages.keys.include? i), "Did not report missing #{i} on finishing game"
    end
  end

  test "should not save without info once finished" do
    g = Game.new
    g.status = "finished"
    assert_not g.save
    info = [:start_time, :expansion_turmoil, :expansion_colonies, :expansion_prelude,
            :expansion_venus_next, :expansion_hellas, :expansion_elysium, :expansion_advanced,
            :drafting, :starting_player, :cap_oxygen, :cap_ocean, :cap_temperature, :end_time, :winner]
    info.each do |i|
      assert (g.errors.messages.keys.include? i), "Did not report missing #{i} on finished game"
    end
  end

  test "should save finished game with all info" do
    g = games(:GameOne)
    milestones_missing = 5 - g.milestones.length
    awards_missing = 5 - g.awards.length
    milestones_missing.times { g.milestones.build }
    awards_missing.times { g.awards.build }
    assert g.save
  end
end
