# == Schema Information
#
# Table name: awards
#
#  id         :integer          not null, primary key
#  name       :string
#  financed   :boolean          default("0")
#  owner_id   :integer
#  game_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  second_id  :integer
#

require 'test_helper'

class AwardTest < ActiveSupport::TestCase
  test "should not save award without game" do
    aw = Award.new
    assert_not aw.save
  end

  test "should not have owner if not financed" do
    aw = awards(:AwardOne) # not financed
    p = players(:PlayerOne)
    aw.owner = p
    assert_not aw.save
  end

  test "should not be financed and not have an owner" do
    aw = awards(:AwardOne) # not financed
    aw.financed = true
    assert_not aw.save
  end

  test "should not have second owner if not more than two players" do
    aw = awards(:AwardOne) # not financed
    aw.financed = true
    aw.owner = players(:PlayerOne)
    aw.second = players(:PlayerTwo)
    assert_not aw.save
  end

  test "should not have only one owner if more than two players" do
    aw = awards(:AwardTwo) # financed, owner: PlayerOne
    assert_not aw.save
  end

  test "should not have owner and second equal" do
    aw = awards(:AwardTwo) # financed, owner: PlayerOne
    aw.second = aw.owner
    assert_not aw.save
  end

  test "should not have owner that is not player" do
    aw = awards(:AwardTwo) # financed, owner: PlayerOne
    aw.owner = players(:PlayerFour)
    assert_not aw.save
  end

  test "should not have second that is not player" do
    aw = awards(:AwardTwo) # financed, owner: PlayerOne
    aw.second = players(:PlayerFour)
    assert_not aw.save
  end
end
