# == Schema Information
#
# Table name: milestones
#
#  id         :integer          not null, primary key
#  name       :string
#  reached    :boolean
#  owner_id   :integer
#  game_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class MilestoneTest < ActiveSupport::TestCase
  test "should not save milestone without game" do
    m = Milestone.new
    assert_not m.save
  end

  test "should not have owner if not reached" do
    m = milestones(:MilestoneTwo)  # not reached
    m.owner = players(:PlayerOne)
    assert_not m.save
  end

  test "should not be reached and not have an owner" do
    m = milestones(:MilestoneTwo)
    m.reached = true
    assert_not m.save
  end

  test "should not have owner that is not player" do
    m = milestones(:MilestoneOne)
    m.owner = players(:PlayerThree)
    assert_not m.save
  end
end
