FROM ruby:2.7.0

LABEL name=terraformingtracker

EXPOSE 3080

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

# need update for sqlite3, unsure about upgrade actually
RUN apt-get update
#RUN apt-get upgrade -yqq
# install sqlite3
RUN apt-get install -y sqlite3
# install nodejs v14 - v16 throws errors with node-sass later
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - 
RUN apt-get install -y nodejs
# install yarn
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null
RUN echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

# make installs possible before copying everything
COPY Gemfile* package.json yarn.lock babel.config.js config.ru postcss.config.js Rakefile /usr/src/app/
WORKDIR /usr/src/app
#ENV RAILS_ENV production
#RUN bundle config set without 'development test'
RUN gem install bundler:2.3.13
RUN bundle install

RUN yarn

COPY . /usr/src/app/

# build database
RUN bundle e rails db:setup

# remove old server PIDs
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# main process
CMD ["rails", "server", "-b", "0.0.0.0", "-p", "3080"]
