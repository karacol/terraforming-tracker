Rails.application.routes.draw do
  root 'games#index'
  resources :games do
    resources :setup_steps, controller: 'game_setup_steps' do
      put :back
    end
    resources :eval_steps, controller: 'game_eval_steps' do
      put :back
    end
  end
  resources :players
  resources :setups
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
