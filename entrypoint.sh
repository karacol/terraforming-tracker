#!/bin/bash
set -e

# remove potentially pre-existing server.pid for rails
rm -f /usr/src/app/tmp/pids/server.pid

# exec contaier's main process (CMD in Dockerfile)
exec "$@"
