// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

// chartkick requirements
require("chartkick")
require("chart.js")

// add javascript code here to be available to all views

// count number of checked checkboxes in a group of checkboxes
function count_checked(group) {
    var checked = 0;
    for (let member of group) {
        if (member.checked) {
            checked++;
        }
    }
    return checked;
}

// disable all other elements in a group once max_num are checked
function force_max(element, max_num, group) {
    var no_checked = count_checked(group);
    if (no_checked == max_num) {
        for (let member of group) {
          if (member.checked==false) {
            member.disabled = true;
        }
      }
    }
    else {
        for (let member of group) {
          member.disabled = false;
      }
    }
}

// enable all other elements belonging to the same group when checked
function activate_on_check(element, group) {
    if (element.checked) {
        for (let member of group) {
            var data = JSON.parse(member.dataset.checkedenable);
            if (data["toggle"]){
                member.disabled = false;
            }
        }
    }
    else {
        for (let member of group) {
            var data = JSON.parse(member.dataset.checkedenable);
            if (data["toggle"]){
                member.disabled = true;
            }
        }
    }
}

function select_group(type, name, checkboxes) {
    var group = [];
    for (let box of checkboxes) {
        if (type == "forcemax") {
            var data = JSON.parse(box.dataset.forcemax);
        }
        else if (type == "checkedenable") {
            var data = JSON.parse(box.dataset.checkedenable);
        }
        var comp = data["group"];
        if (comp == name) {
            group.push(box);
        }
    }
    return group;
}

function show_hide(element, divs) {
    tag = element.dataset.showhide;
    for (let d of divs) {
        if (d.dataset.showhide == tag) {
            if (d.style.display == 'none') {
                d.style.display = 'block';
            } else if (d.style.display == 'block') {
                d.style.display = 'none';
            }            
        }
    }
}

function check_all_caps(element, caps, submit_btn, hidden) {
    var given = 0;
    var max_gen = 0;
    for (let c of caps) {
        if (c.value != ""){
            given ++;
            if (c.value > max_gen){
                max_gen = c.value;
            }
        }
    }
    if (given == 3){
        submit_btn.dataset.confirm = "You have entered three environmental caps, which will end the game. Is this correct?";
        for (let h of hidden) {
            if (h.dataset.caphidden == "status") {
                h.value = "finishing";
            }
            else if (h.dataset.caphidden == "generations") {
                h.value = max_gen;
            }
        }
    }
    else {
        submit_btn.dataset.confirm = "";
        for (let h of hidden) {
            if (h.dataset.caphidden == "status") {
                h.value = "running";
            }
            else if (h.dataset.caphidden == "generations") {
                h.value = "";
            }
        }
    }
}

window.addEventListener("turbolinks:load", () => {
    const checkboxes = document.querySelectorAll("[data-forcemax]");
    checkboxes.forEach((element) => {
        element.addEventListener("click", function() {
            var data = JSON.parse(element.dataset.forcemax);
            force_max(element, data["max_num"], select_group("forcemax", data["group"], checkboxes));
        });
    });
    // on each reload, make sure the elements are enabled and disabled as intended
    if (checkboxes.length > 0){
        var data = JSON.parse(checkboxes[0].dataset.forcemax);
        force_max(checkboxes[0], data["max_num"], select_group("forcemax", data["group"], checkboxes));
    }
});

window.addEventListener("turbolinks:load", () => {
    const checkboxes = document.querySelectorAll("[data-checkedenable]");
    checkboxes.forEach((element) => {
        var data = JSON.parse(element.dataset.checkedenable);        
        if (data["toggle"]==false){  // objects with toggle=true are to be enabled/disabled, but shall not initiate the toggling
            element.addEventListener("click", function() {
                activate_on_check(element, select_group("checkedenable", data["group"], checkboxes));
            });
        }
    });
});

window.addEventListener("turbolinks:load", () => {
    const divs = document.querySelectorAll("div[data-showhide]");
    const buttons = document.querySelectorAll("input[data-showhide]");
    buttons.forEach((element) => {
        element.addEventListener("click", function() {
            show_hide(element, divs);
        });
    });
});

window.addEventListener("turbolinks:load", () => {
    const cap_fields = document.querySelectorAll("input[data-cap]");
    const submit_btn = document.querySelector("input[data-capsubmit]");
    const hidden_fields = document.querySelectorAll("input[data-caphidden]")
    cap_fields.forEach((element) => {
        element.addEventListener("input", function() {
            check_all_caps(element, cap_fields, submit_btn, hidden_fields);
        });
    });
});

// Reload when a form error ocurred (status 422)
// event = [HTMLDocument, "Unprocessable Entity" (text for status 422), XMLHttpRequest]
window.addEventListener("ajax:error", (event) => {
    if (event.detail[2].status !== 422) { return }
    document.body = event.detail[0].body
    Turbolinks.dispatch("turbolinks:load")
    scrollTo(0, 0)
  })