class PlayersController < ApplicationController
    def index
      @players = Player.all
    end

    def new
        @player = Player.new
    end

    def create
      @player = Player.new(player_params_create)
      if @player.save
        redirect_to root_path
      else
        render :new, status: 422
      end
    end

    private
      def player_params_create
        params.require(:player).permit(:name)
      end


end
