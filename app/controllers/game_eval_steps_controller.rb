class GameEvalStepsController < ApplicationController
    include Wicked::Wizard

    steps :_final_generations, :_final_production, :_final_milestones_awards, :_final_tiles, :_final_score, :_confirm_evaluation

    @@setup_idx = 0

    def show
        @game = Game.find(params[:game_id])
        if params[:setup_id]
            @setup = Setup.find(params[:setup_id])
        end
        render_wizard
    end

    def update
        @game = Game.find(params[:game_id])
        case step
        when :_final_generations, :_final_milestones_awards
            @game.assign_attributes(game_params_update)
            if @game.save(context: step)
                render_wizard(@game, {context: step}, {:setup_id=>@game.setups[@@setup_idx].id})
            else
                render_wizard(@game, {context: step, status: 422})
            end
        when :_final_production, :_final_score
            @setup = @game.setups[@@setup_idx]
            @setup.assign_attributes(setup_params_update)
            if @setup.save(context: step)
                rot_setup_idx
                unless @@setup_idx==0  # more setups to do
                    forward_in_loop
                else
                    render_wizard @setup, context: step
                end
            else
                render_wizard @setup, {context: step, status: 422}
            end
        when :_final_tiles
            @setup = @game.setups[@@setup_idx]
            @setup.assign_attributes(setup_params_update)
            if @setup.save(context: step)
                rot_setup_idx
                unless @@setup_idx==0  # finished setups
                    forward_in_loop
                else
                    # forward to loop
                    render_wizard(@setup, {context: step}, {:setup_id=>@game.setups[@@setup_idx].id})
                end
            else
                render_wizard @setup, {context: step, status: 422}
            end
        end
    end

    # one step back without saving the parameters
    def back
        @game = Game.find(params[:game_id])
        case step
        when :_final_production, :_final_tiles
            @setup = @game.setups[@@setup_idx]
            unless @@setup_idx == 0
                back_in_loop
            else
                jump_to(@previous_step)
                render_wizard
            end
        when :_final_score
            @setup = @game.setups[@@setup_idx]
            unless @@setup_idx == 0
                back_in_loop
            else
                back_to_loop
            end
        when :_final_milestones_awards
            back_to_loop
        else
            jump_to(@previous_step)
            render_wizard
        end
    end    

    private
    def game_params_update
        params.require(:game).permit(:generations,
                                     milestones_attributes: [:id, :reached, :owner_id],
                                     awards_attributes: [:id, :financed, :owner_id, :second_id])
    end
    def setup_params_update
        params.require(:setup).permit(:score, :green_tiles, :city_tiles, :other_tiles,
                                      :cash_production, :steel_production, :titanium_production,
                                      :plant_production, :energy_production, :heat_production)
    end
    def rot_setup_idx
        @@setup_idx = (@@setup_idx < @game.setups.length-1) ? @@setup_idx+1 : 0
    end
    def back_in_loop
        # decrease setup idx
        @@setup_idx -= 1
        # render with new setup idx
        jump_to(step)
        render_wizard(nil, {}, {:setup_id=>@game.setups[@@setup_idx].id})
    end
    def back_to_loop
        # last setup
        @@setup_idx = @game.setups.length-1
        jump_to(@previous_step)
        render_wizard(nil, {}, {:setup_id=>@game.setups[@@setup_idx].id})
    end
    def forward_in_loop
        jump_to(step)
        render_wizard(@setup, {context: step}, {:setup_id=>@game.setups[@@setup_idx].id})
    end
end
