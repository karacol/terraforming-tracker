class GamesController < ApplicationController
  def index
    @games = Game.all
  end

  def show
    @game = Game.find(params[:id])
  end

  def new
    @game = Game.new
    5.times { @game.milestones.build }
    5.times { @game.awards.build }
  end

  def create
    player_ids = params[:game][:player_ids]
    @game = Game.new(game_params_create)
    5.times { @game.milestones.build }
    5.times { @game.awards.build }
    player_ids.each do |player_id|
      unless player_id.blank?
        @game.setups.build(player_id: player_id)
      end
    end
    if @game.save
      redirect_to game_setup_steps_path(@game)
    else
      render :new, status: 422
    end
  end

  def edit
    @game = Game.find(params[:id])
  end

  def update
    @game = Game.find(params[:id])
    if params[:game][:status] == "finished"
      idx_of_highest_score = @game.setups.map {|s| s[:score]}.each_with_index.max[1]
      @game.setups[idx_of_highest_score].winner = true
      unless @game.setups[idx_of_highest_score].save
        flash.now[:alert] = "Couldn't end the game!"
        redirect_to game_eval_steps_path(@game)
      end
    end
    if @game.update(game_params_update)
      if @game.setting_up?
        redirect_to game_setup_steps_path(@game)
      elsif @game.running?
        redirect_to edit_game_path(@game)
      elsif @game.finishing?
        redirect_to game_eval_steps_path(@game)
      else  # finished
        redirect_to @game
      end
    else
      render :edit, status: 422
    end
  end

  def destroy
    @game = Game.find(params[:id])
    @game.destroy

    redirect_to root_path
  end

  private
    def game_params_create
        params.require(:game).permit()
    end

    def game_params_update
        params.require(:game).permit(:start_time, :end_time, :status, :cap_oxygen, :cap_ocean, :cap_temperature, :cap_venus, :generations,
                                     setups_attributes: [:id, :prelude_three], colonies: [])
    end
end
