class GameSetupStepsController < ApplicationController
    include Wicked::Wizard
    before_action :set_steps
    before_action :setup_wizard

    @@setup_idx = 0

    def show
        @game = Game.find(params[:game_id])
        if params[:setup_id]
            @setup = Setup.find(params[:setup_id])
        end
        render_wizard
    end

    def update
        @game = Game.find(params[:game_id])
        case step
        when :_choose_expansions
            if params[:game][:expansion_venus_next]=="1" and @game.milestones.count==5
                # add one more milestone + award
                @game.milestones.build
                @game.awards.build
            end
            @game.assign_attributes(game_params_update)
            if @game.save(context: step)
                render_wizard @game, context: step
            else
                render_wizard @game, {context: step, status: 422}
            end
        when :_choose_setup
            @setup = @game.setups[@@setup_idx]
            @setup.assign_attributes(setup_params_update)
            if @setup.save(context: step)
                rot_setup_idx
                unless @@setup_idx==0  # finished setups
                    jump_to(:_choose_setup)
                    render_wizard(@setup, {context: step}, {:setup_id=>@game.setups[@@setup_idx].id})
                else
                    render_wizard @setup, context: step
                end
            else
                render_wizard @setup, {context: step, status: 422}
            end
        when :_choose_milestones_awards
            @game.assign_attributes(game_params_update)
            if @game.save(context: step)
                unless @game.expansion_colonies  # go to choose_setup
                    render_wizard @game, {context: step}, {:setup_id=>@game.setups[@@setup_idx].id}
                else
                    render_wizard @game, context: step
                end
            else
                render_wizard @game, {context: step, status: 422}
            end
        when :_choose_colonies
            @game.assign_attributes(game_params_update)
            if @game.save(context: step)
                render_wizard(@game, {context: step}, {:setup_id=>@game.setups[@@setup_idx].id})
            else
                render_wizard @game, {context: step, status: 422}
            end
        else
            @game.assign_attributes(game_params_update)
            if @game.save(context: step)
                render_wizard @game, context: step
            else
                render_wizard @game, {context: step, status: 422}
            end
        end
    end

    def back
        @game = Game.find(params[:game_id])
        case step
        when :_choose_setup
            @setup = @game.setups[@@setup_idx]
            unless @@setup_idx == 0
                # decrease setup idx
                @@setup_idx -= 1
                # render with new setup idx
                jump_to(:_choose_setup)
                render_wizard(nil, {}, {:setup_id=>@game.setups[@@setup_idx].id})
            else
                jump_to(@previous_step)
                render_wizard
            end
        else
            jump_to(@previous_step)
            render_wizard
        end
    end

    private
    def game_params_update
        params.require(:game).permit(:drafting, :starting_player_id,
                                     :expansion_turmoil, :expansion_venus_next, :expansion_hellas,
                                     :expansion_elysium, :expansion_colonies, :expansion_prelude,
                                     :expansion_advanced,
                                     colonies: [], milestones_attributes: [:id, :name], awards_attributes: [:id, :name])
    end
    def setup_params_update
        params.require(:setup).permit(:company, :prelude_one, :prelude_two)
    end
    def set_steps
        @game = Game.find(params[:game_id])
        unless @game.expansion_colonies
            self.steps = [:_choose_starting_player, :_choose_expansions, :_choose_milestones_awards, :_choose_setup, :_confirm_setup]
        else
            self.steps = [:_choose_starting_player, :_choose_expansions, :_choose_milestones_awards, :_choose_colonies, :_choose_setup, :_confirm_setup]
        end
    end
    def rot_setup_idx
        @@setup_idx = (@@setup_idx < @game.setups.length-1) ? @@setup_idx+1 : 0
    end
end
