class SetupsController < ApplicationController
    def show
        @setup = Setup.find(params[:id])
      end

    def destroy
        @setup = Setup.find(params[:id])
        @setup.destroy
    
        redirect_to root_path
    end
end
