# == Schema Information
#
# Table name: games
#
#  id                   :integer          not null, primary key
#  generations          :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  start_time           :datetime
#  end_time             :datetime
#  status               :string           default("setting up")
#  expansion_turmoil    :boolean
#  expansion_colonies   :boolean
#  expansion_prelude    :boolean
#  expansion_venus_next :boolean
#  cap_oxygen           :integer
#  cap_ocean            :integer
#  cap_temperature      :integer
#  cap_venus            :integer
#  expansion_hellas     :boolean
#  expansion_elysium    :boolean
#  expansion_advanced   :boolean
#  drafting             :boolean
#  colonies             :text
#  starting_player_id   :integer
#

class Game < ApplicationRecord
    include Editable
    has_many :setups, dependent: :destroy
    has_many :players, through: :setups
    has_many :milestones, dependent: :destroy
    has_many :awards, dependent: :destroy
    belongs_to :starting_player, class_name: "Player", optional: true

    accepts_nested_attributes_for :milestones
    accepts_nested_attributes_for :awards
    accepts_nested_attributes_for :setups
    
    serialize :colonies, Array

    validates :setups, length: { in: 1..5 , too_long: "%{count} players is the maximum allowed",
                                            too_short: "%{count} player is the minimum required" }
    validates :milestones, :awards, length: { in: 5..6, too_long: "%{count} is the maximum allowed",
                                                        too_short: "%{count} is the minimum required" }
    validates :starting_player, presence: true, on: :_choose_starting_player
    validates :expansion_turmoil, :expansion_colonies, :expansion_prelude, :expansion_venus_next,
              :expansion_hellas, :expansion_elysium, :expansion_advanced, :drafting,
              inclusion: {in: [true, false], message: "must be a boolean"},
              if: -> {["running", "finishing", "finished"].include? status}
    validates :start_time, :starting_player, presence: true, if: -> {["running", "finishing", "finished"].include? status}
    validates :colonies, presence: true, if: -> { expansion_colonies and ["running", "finishing", "finished"].include? status}
    validates :cap_oxygen, :cap_ocean, :cap_temperature, presence: true, if: -> { ["finishing", "finished"].include? status }
    validates :end_time, :winner, presence: true, if: :finished?
    validate :time
    validate :unique_players
    validate :hellas_elysium, on: :_choose_expansions
    validates_each :milestones, :awards, on: :_choose_milestones_awards do |record, attr, values|
        if Set.new(values.collect{|v| v.name}).length < values.length
            record.errors.add(attr, "must be different from each other")
        end
    end

    def time
        if end_time.present?
            unless end_time > start_time
                errors.add(:end_time, "must be later than start time")
            end
        end
    end

    def unique_players
        if Set.new(setups.collect{|s| s.player_id}).length < setups.length
            errors.add(:players, "must be different from each other")
        end
    end

    def hellas_elysium
        if expansion_hellas and expansion_elysium
            errors.add(:expansion_hellas, "cannot be played together with Elysium")
            errors.add(:expansion_elysium, "cannot be played together with Hellas")
        end
    end

    def winner
        winner_setups = setups.where(winner: true)
        if winner_setups.length > 0
            return winner_setups[0].player
        end
        return nil
    end

    def get_expansions_names()
        # This would actually be much nicer if we had a keyed hash... And we could just check the value and append the key.
        expansions = []
        if expansion_turmoil
            expansions << "Turmoil"
        end
        if expansion_colonies
            expansions << "Colonies"
        end
        if expansion_prelude
            expansions << "Prelude"
        end
        if expansion_hellas
            expansions << "Hellas"
        end
        if expansion_elysium
            expansions << "Elysium"
        end
        if expansion_venus_next
            expansions << "Venus Next"
        end
        if expansion_advanced
            expansions << "Advanced"
        end
        return expansions
    end

    def get_available_prelude_cards()
        prelude_list = ["#P01 Allied Banks", "#P02 Aquifer Turbines", "#P03 Biofuels", "#P04 Biolab",
                        "#P05 Biosphere Support", "#P06 Business Empire", "#P07 Dome Farming", "#P08 Donation",
                        "#P09 Early Settlement", "#P10 Ecology Experts", "#P11 Excentric Sponsor", "#P12 Experimental Forest",
                        "#P13 Galilean Mining", "#P14 Great Aquifer", "#P15 Huge Asteroid", "#P16 Io Research Outpost",
                        "#P17 Loan", "#P18 Martian Industries", "#P19 Metal-rich Asteroid", "#P20 Metals Company",
                        "#P21 Mining Operations", "#P22 Mohole", "#P23 Mohole Excavation", "#P24 Nitrogen Shipment",
                        "#P25 Orbital Construction Yard", "#P26 Polar Industries", "#P27 Power Generation", "#P28 Research Network",
                        "#P29 Self-Sufficient Settlement", "#P30 Smelting Plant", "#P31 Society Support", "#P32 Supplier",
                        "#P33 Supply Drop", "#P34 UNMI Contractor", "#P35 Acquired Space Agency"]
        prelude_cards_selected_in_setup = setups.collect {|s| s.prelude_cards.collect {|k, v| v unless k=="three"}}.flatten.compact
        available_prelude_cards = prelude_list - prelude_cards_selected_in_setup
        return available_prelude_cards
    end

    def third_prelude?
        unless setups.collect{|s| s.prelude_three}.compact.length > 0
            return false
        else
            return true
        end
    end

end
