# == Schema Information
#
# Table name: awards
#
#  id         :integer          not null, primary key
#  name       :string
#  financed   :boolean          default("0")
#  owner_id   :integer
#  game_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  second_id  :integer
#

class Award < ApplicationRecord
  belongs_to :owner, class_name: "Player", optional: true
  belongs_to :game
  belongs_to :second, class_name: "Player", optional: true

  validates :game, presence: true
  validate :owner_validation, :second_validation

  def owner_validation
    if financed
      unless owner.present?
        errors.add(:owner, "can't be blank for financed award")
      end
    else
      if owner.present?
        unless game.present? and game.players.include? owner
          errors.add(:owner, "must be one of the players")
        end
        errors.add(:owner, "must be blank for non-financed award")
      end
    end
  end

  def second_validation
    if game.present? and game.players.length > 2
      if financed
        if not second.present?
          errors.add(:second, "can't be blank for financed award in game with more than 2 players")
        elsif owner == second
          errors.add(:second, "must be different from first place for financed award")
        elsif not game.players.include? second
          errors.add(:second, "must be one of the players")
        end
      else
        if second.present?
          errors.add(:second, "must be blank for non-financed award")
        end
      end
    else
      if second.present?
        errors.add(:second, "must be blank in game with no more than 2 players")
      end
    end
  end    

  def get_selected(idx)
    basic = ["Landlord", "Scientist", "Banker", "Thermalist", "Miner"]
    hellas = ["Cultivator", "Magnate", "Space Baron", "Excentric", "Contractor"]
    elysium = [ "Celebrity", "Industrialist", "Desert Settler", "Estate Dealer", "Benefactor"]
    venus = "Venuphile"
    if name
      return name
    else
      unless idx == 5
        if game.expansion_hellas
          return hellas[idx]
        elsif game.expansion_elysium
          return elysium[idx]
        else
          return basic[idx]
        end
      else
        return venus
      end
    end
  end
end
