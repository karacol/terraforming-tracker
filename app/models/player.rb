# == Schema Information
#
# Table name: players
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Player < ApplicationRecord
    has_many :setups
    has_many :games, through: :setups
    has_many :milestones, foreign_key: :owner
    has_many :awards, foreign_key: :owner

    validates :name, presence: true
    validates :name, uniqueness: true

    def winner?(game_id)
        # look up whether this player won that game
        if games.exists?(game_id)
            game = games.find(game_id)
        else
            return false
        end
        winner = game.winner
        if winner.present? and winner.id == id
            return true
        else
            return false
        end
    end

    def get_all_wins()
        won_setups = setups.where(winner: true)
        wins = []
        won_setups.each {|w| wins << w.game}
        return wins
    end
end
