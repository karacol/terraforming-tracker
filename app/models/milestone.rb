# == Schema Information
#
# Table name: milestones
#
#  id         :integer          not null, primary key
#  name       :string
#  reached    :boolean
#  owner_id   :integer
#  game_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Milestone < ApplicationRecord
  belongs_to :owner, class_name: "Player", optional: true
  belongs_to :game

  validates :game, presence: true
  validate :owner_validation

  def owner_validation
    if reached
      if owner.present?
        if game.present? and not game.players.include? owner
          errors.add(:owner, "must be one of the players")
        end
      else
        errors.add(:owner, "can't be blank for reached milestone")
      end
    else
      if owner.present?
        errors.add(:owner, "must be blank for unreached milestone")
      end
    end
  end

  def get_selected(idx)
    basic = ["Terraformer", "Mayor", "Gardener", "Builder", "Planner"]
    hellas = ["Diversifier", "Tactitian", "Polar Explorer", "Energizer", "Rim Settler"]
    elysium = ["Generalist", "Specialist", "Ecologist", "Tycoon", "Legend"]
    venus = "Hoverlord"
    if name
      return name
    else
      unless idx == 5
        if game.expansion_hellas
          return hellas[idx]
        elsif game.expansion_elysium
          return elysium[idx]
        else
          return basic[idx]
        end
      else
        return venus
      end
    end
  end
end
