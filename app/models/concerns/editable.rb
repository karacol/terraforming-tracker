module Editable
    extend ActiveSupport::Concern

    VALID_STATUSES = ['setting up', 'running', 'finishing', 'finished']

    included do
        validates :status, inclusion: { in: VALID_STATUSES}
    end

    def setting_up?
        status == 'setting up'
    end

    def running?
        status == 'running'
    end

    def finishing?
        status == 'finishing'
    end

    def finished?
        status == 'finished'
    end
end