# == Schema Information
#
# Table name: setups
#
#  id                  :integer          not null, primary key
#  company             :string
#  score               :integer
#  player_id           :integer          not null
#  game_id             :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  green_tiles         :integer
#  city_tiles          :integer
#  cash_production     :integer
#  steel_production    :integer
#  titanium_production :integer
#  plant_production    :integer
#  energy_production   :integer
#  heat_production     :integer
#  prelude_cards       :text
#  winner              :boolean          default("0")
#  other_tiles         :integer
#

class Setup < ApplicationRecord
    belongs_to :game
    belongs_to :player

    store :prelude_cards, accessors: [:one, :two, :three], code: JSON, prefix: "prelude"

    validates :game, :player, presence: true
    validates :green_tiles, numericality: { greater_than_or_equal_to: 0 }, on: :_final_tiles
    validates :city_tiles, numericality: { greater_than_or_equal_to: 0 }, on: :_final_tiles
    validates :other_tiles, numericality: { greater_than_or_equal_to: 0 }, on: :_final_tiles
    with_options on: :_final_production do |s|
        s.validates :cash_production, numericality: { greater_than_or_equal_to: -5 }
        prods = [:steel_production, :titanium_production, :plant_production, :energy_production, :heat_production]
        prods.each do |p|
            s.validates p, numericality: { greater_than_or_equal_to: 0 }
        end
    end
    validates :score, numericality: { greater_than_or_equal_to: 0 }, on: :_final_score
    validates :company, presence: true, if: -> {game.present? and ["running", "finishing", "finished"].include? game.status}
    validates :prelude_cards, presence: true, if: -> {game.present? and game.expansion_prelude and ["running", "finishing", "finished"].include? game.status}
    validate :end_of_setup, on: :_choose_setup
    validate :running, on: :update, if: -> {game.running?}

    def end_of_setup
        # company
        unless company.present?
            errors.add(:company, "can't be blank")
        else
            unless company == "Starting Corporation"
                game.players.each do |p|
                    unless p == player
                        p_setup = game.setups.find_by player: p
                        if p_setup.company == company
                            errors.add(:company, "is already played by #{p.name}")
                        end
                    end
                end
            end
        end
        # prelude
        if game.expansion_prelude
            # prelude 1
            unless prelude_one.present?
                errors.add(:prelude_one, "can't be blank")
            else
                p = check_other_players_prelude(prelude_one)
                unless p.nil?
                    errors.add(:prelude_one, "is already used by #{p.name}")
                end
            end
            # prelude 2
            unless prelude_two.present?
                errors.add(:prelude_two, "can't be blank")
            else
                if prelude_one == prelude_two
                    errors.add(:prelude_two, "must be different from other selected prelude card")
                else
                    p = check_other_players_prelude(prelude_two)
                    unless p.nil?
                        errors.add(:prelude_two, "is already used by #{p.name}")
                    end
                end
            end
        end
    end

    def running
        if company == "Valley Trust"
            unless prelude_three.present?
                errors.add(:prelude_three, "can't be blank")
            else
                p = check_other_players_prelude(prelude_three)
                unless p.nil?
                    errors.add(:prelude_three, "is already used by #{p.name}")
                end
            end
        else
            if prelude_three.present?
                errors.add(:prelude_three, "must be blank")
            end
        end
    end

    private
    def check_other_players_prelude(card_to_check)
        game.players.each do |p|
            unless p == player
                p_setup = game.setups.find_by player: p
                if p_setup.prelude_cards.values.include? card_to_check
                    return p
                end
            end
        end
        return nil
    end        
end
